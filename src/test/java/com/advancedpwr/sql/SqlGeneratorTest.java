package com.advancedpwr.sql;

import java.lang.reflect.Method;
import java.util.List;

import junit.framework.TestCase;

public class SqlGeneratorTest extends TestCase
{
	public void testPrintSchema()
	{
		SqlGenerator generator = new SqlGenerator();
		
		
		generator.setData( new Customer() );
		assertEquals( "CREATE TABLE CUSTOMER (\n" + 
				"ID IDENTITY,\n" + 
				"CREATEDDATE TIMESTAMP,\n" + 
				"FIRSTNAME VARCHAR(100),\n" + 
				"GOOD INTEGER,\n" + 
				"LASTNAME VARCHAR(100),\n" + 
				"LASTUPDATED TIMESTAMP )", generator.toCreateTable() );
		System.out.println( "DROP TABLE " + generator.getTableName() + ";\n");
		System.out.println( generator.toCreateTable()  + ";");
	}
	
	public void testInvokeMethod_error()
	{
		SqlGenerator generator = new SqlGenerator()
		{
			protected Object invokeMethodRaw( Method method ) throws Exception
			{
				throw new Exception( "Bad stuff" );
			}
		};
		
		try
		{
			generator.invokeMethod( null );;
			fail( "Should have thrown an Exception" );
		}
		catch( Exception e )
		{
			assertTrue( e instanceof RuntimeException );
		}
	}
	
	public void testColumns()
	{
		SqlGenerator gen = new SqlGenerator();
		Customer template = new Customer()
		{

			public IdColumn primaryKey()
			{
				return null;
			}
			
		};
		template.setFirstName( "James" );
		gen.setData( template );
		List<Column> cols = gen.populatedColumns();
		for ( Column column : cols )
		{
			System.out.println( column.getName() + " " + column.getValue() );
		}
		
//		Good false
//		FirstName James
//		LastName null
//		CreatedDate null
//		LastUpdated null
	}
	

}
