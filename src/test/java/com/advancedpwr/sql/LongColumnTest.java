package com.advancedpwr.sql;

import java.sql.ResultSet;

import org.easymock.EasyMock;

import junit.framework.TestCase;

public class LongColumnTest extends TestCase
{
	public void testMappedValue() throws Exception
	{
		LongColumn column = new LongColumn();
		column.setName( "id" );
		ResultSet rs = EasyMock.createMock( ResultSet.class );
		EasyMock.expect( rs.getLong( "id" ) ).andReturn( new Long(0) );
		EasyMock.expect( rs.getLong( "id" ) ).andReturn( new Long(12) );
		EasyMock.expect( rs.getLong( "id" ) ).andReturn( new Long(5) );
		EasyMock.replay( rs );	
		assertEquals( Long.valueOf( 0 ), (Long)column.mapValue( rs ) );
		assertEquals( Long.valueOf( 12 ), (Long)column.mapValue( rs ) );
		assertEquals( Long.valueOf( 5 ), (Long)column.mapValue( rs ) );
		
	}
}
