package com.advancedpwr.sql;

import junit.framework.TestCase;

public class SqlLoaderTest extends TestCase
{
	public void testPopulate_error()
	{
		SqlLoader loader = new SqlLoader();
		
		try
		{
			loader.populate();
			fail( "Should have thrown an Exception" );
		}
		catch( Exception e )
		{
			assertTrue( e instanceof RuntimeException );
		}
	}
	
	
	
}
