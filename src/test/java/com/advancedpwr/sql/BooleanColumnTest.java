package com.advancedpwr.sql;

import java.sql.ResultSet;

import org.easymock.EasyMock;

import junit.framework.TestCase;

public class BooleanColumnTest extends TestCase
{
	public void testMappedValue() throws Exception
	{
		BooleanColumn column = new BooleanColumn();
		column.setName( "flag" );
		ResultSet rs = EasyMock.createMock( ResultSet.class );
		EasyMock.expect( rs.getObject( "flag" ) ).andReturn( new Integer(0) );
		EasyMock.expect( rs.getObject( "flag" ) ).andReturn( new Integer(1) );
		EasyMock.replay( rs );	
		assertFalse( (Boolean)column.mapValue( rs ) );
		assertTrue( (Boolean)column.mapValue( rs ) );
	}
}
