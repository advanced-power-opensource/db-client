package com.advancedpwr.sql;

import java.net.URL;

public class ComplexBean
{
	protected String[] fieldStringArray;
	protected int[] fieldIntArray;
	protected byte[] fieldByteArray;
	
	protected URL fieldURL;

	protected ComplexBean fieldDefault;
	
	protected boolean fieldFlag;

	public String[] getStringArray()
	{
		return fieldStringArray;
	}

	public void setStringArray( String[] stringArray )
	{
		fieldStringArray = stringArray;
	}

	public int[] getIntArray()
	{
		return fieldIntArray;
	}

	public void setIntArray( int[] intArray )
	{
		fieldIntArray = intArray;
	}

	public byte[] getByteArray()
	{
		return fieldByteArray;
	}

	public void setByteArray( byte[] byteArray )
	{
		fieldByteArray = byteArray;
	}

	public URL getURL()
	{
		return fieldURL;
	}

	public void setURL( URL uRL )
	{
		fieldURL = uRL;
	}

	public ComplexBean getDefault()
	{
		return fieldDefault;
	}

	public void setDefault( ComplexBean default1 )
	{
		fieldDefault = default1;
	}

	public boolean getFlag()
	{
		return fieldFlag;
	}

	public void setFlag( boolean flag )
	{
		fieldFlag = flag;
	}
	
	public URL getDefaultURL()
	{
		return getDefault().getURL();
	}
	
	public void setDefaultURL( URL url )
	{
		getDefault().setURL( url );
	}
}
