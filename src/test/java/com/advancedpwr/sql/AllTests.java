package com.advancedpwr.sql;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests
{

	public static Test suite()
	{
		TestSuite suite = new TestSuite( AllTests.class.getName() );
		//$JUnit-BEGIN$
		suite.addTestSuite( BooleanColumnTest.class );
		suite.addTestSuite( DbClientTest.class );
		suite.addTestSuite( LongColumnTest.class );
		suite.addTestSuite( SqlAccessorTest.class );
		suite.addTestSuite( SqlGeneratorTest.class );
		suite.addTestSuite( SqlLoaderTest.class );
		suite.addTestSuite( SqlWhereGeneratorTest.class );
		//$JUnit-END$
		return suite;
	}

}
