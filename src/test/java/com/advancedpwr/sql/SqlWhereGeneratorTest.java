/**
 * 
 */
package com.advancedpwr.sql;

import junit.framework.TestCase;

/**
 * @author Matthew Avery, mavery@advancedpwr.com
 * CreatedStamped: Oct 12, 2011
 *
 */
public class SqlWhereGeneratorTest extends TestCase
{
	public void testToWhereClause()
	{
		Customer customer = new Customer();
		customer.setFirstName( "Matt" );
		SqlWhereGenerator generator = new SqlWhereGenerator();
		generator.setData( customer );
		assertEquals(  " WHERE FIRSTNAME = ?", generator.toWhereClause() );
		customer.setId( 1234 );
		assertEquals(  " WHERE FIRSTNAME = ? AND ID = ?", generator.toWhereClause() );
	}
	
	public void testToWhereClauseMessage()
	{
		Customer customer = new Customer();
		customer.setFirstName( "Matt" );
		SqlWhereGenerator generator = new SqlWhereGenerator();
		generator.setData( customer );
		assertEquals(  " WHERE FIRSTNAME = 'Matt'", generator.toWhereClauseMessage() );
		customer.setId( 1234 );
		assertEquals(  " WHERE FIRSTNAME = 'Matt' AND ID = '1234'", generator.toWhereClauseMessage() );
	}
}
