/**
 * 
 */
package com.advancedpwr.sql;

import java.util.Date;

import com.advancedpwr.action.generated.InsertCustomerConnectionFactory;
import com.advancedpwr.action.generated.LoadCustomerConnectionFactory;
import com.advancedpwr.action.generated.LoadCustomerOrderByConnectionFactory;
import com.advancedpwr.action.generated.UpdateCustomerConnectionFactory;

import junit.framework.TestCase;

/**
 * @author Matthew Avery, mavery@advancedpwr.com
 * CreatedStamped: Mar 30, 2012
 *
 */
public class DbClientTest extends TestCase
{
	protected Customer createCustomer()
	{
		Customer customer = new Customer();
		customer.setFirstName( "James" );
		customer.setLastName( "Kirk" );
		customer.setGood( true );
		customer.setCreatedDate( new Date() );
		return customer;
	}
	
	public void testInsert() throws Exception
	{
		CustomerDbClient client = new CustomerDbClient();
		client.setConnection( new InsertCustomerConnectionFactory().buildConnection() );
		client.insertRecord( createCustomer() );
	}
	
	public void testLoad() throws Exception
	{
		CustomerDbClient client = new CustomerDbClient();
		client.setConnection( new LoadCustomerConnectionFactory().buildConnection() );
		Customer customer = client.load(1);
	}
	
	public void testUpdate() throws Exception
	{
		CustomerDbClient client = new CustomerDbClient();
		client.setConnection( new UpdateCustomerConnectionFactory().buildConnection() );
		Customer customer = createCustomer();
		customer.setId( 1 );
		customer.setFirstName( "Tiberius" );
		customer.setGood( false );
		client.update( customer );
	}
	
	public void testOrderBy() throws Exception
	{
		CustomerDbClient client = new CustomerDbClient();
		client.setConnection( new LoadCustomerOrderByConnectionFactory().buildConnection() );
		Customer template = new Customer()
		{

			public IdColumn primaryKey()
			{
				return null;
			}
			
		};
		template.setFirstName( "James" );
		
		
		client.setOrderBy( "order by createddate desc" );
		Customer customer = client.loadWhere( template );
	}
}
