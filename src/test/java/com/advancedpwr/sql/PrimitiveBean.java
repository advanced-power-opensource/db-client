/**
 * 
 */
package com.advancedpwr.sql;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * @author Matthew Avery, mavery@advancedpwr.com
 * CreatedStamped: Oct 6, 2011
 *
 */
public class PrimitiveBean extends SimpleRecord
{
	protected boolean fieldBoolean;
	protected int fieldInt;
	protected long fieldLong;
	protected short fieldShort;
	protected float fieldFloat;
	protected double fieldDouble;
	protected char fieldChar;
	protected byte fieldByte;
	
	protected Integer fieldIntegerObj;
	protected Long fieldLongObj;
	protected Short fieldShortObj;
	protected Double fieldDoubleObj;
	protected Float fieldFloatObj;
	
	protected String fieldString;
	protected BigInteger fieldBigInteger;
	protected BigDecimal fieldBigDecimal;
	public boolean isBoolean()
	{
		return fieldBoolean;
	}
	public void setBoolean( boolean b )
	{
		fieldBoolean = b;
	}
	public int getInt()
	{
		return fieldInt;
	}
	public void setInt( int i )
	{
		fieldInt = i;
	}
	public long getLong()
	{
		return fieldLong;
	}
	public void setLong( long l )
	{
		fieldLong = l;
	}
	public short getShort()
	{
		return fieldShort;
	}
	public void setShort( short s )
	{
		fieldShort = s;
	}
	public float getFloat()
	{
		return fieldFloat;
	}
	public void setFloat( float f )
	{
		fieldFloat = f;
	}
	public double getDouble()
	{
		return fieldDouble;
	}
	public void setDouble( double d )
	{
		fieldDouble = d;
	}
	public char getChar()
	{
		return fieldChar;
	}
	public void setChar( char c )
	{
		fieldChar = c;
	}
	public byte getByte()
	{
		return fieldByte;
	}
	public void setByte( byte b )
	{
		fieldByte = b;
	}
	public String getString()
	{
		return fieldString;
	}
	public void setString( String string )
	{
		fieldString = string;
	}
	public BigInteger getBigInteger()
	{
		return fieldBigInteger;
	}
	public void setBigInteger( BigInteger bigInteger )
	{
		fieldBigInteger = bigInteger;
	}
	public BigDecimal getBigDecimal()
	{
		return fieldBigDecimal;
	}
	public void setBigDecimal( BigDecimal bigDecimal )
	{
		fieldBigDecimal = bigDecimal;
	}
	public Integer getIntegerObj()
	{
		return fieldIntegerObj;
	}
	public void setIntegerObj( Integer integerObj )
	{
		fieldIntegerObj = integerObj;
	}
	public Long getLongObj()
	{
		return fieldLongObj;
	}
	public void setLongObj( Long longObj )
	{
		fieldLongObj = longObj;
	}
	public Short getShortObj()
	{
		return fieldShortObj;
	}
	public void setShortObj( Short shortObj )
	{
		fieldShortObj = shortObj;
	}
	public Double getDoubleObj()
	{
		return fieldDoubleObj;
	}
	public void setDoubleObj( Double doubleObj )
	{
		fieldDoubleObj = doubleObj;
	}
	public Float getFloatObj()
	{
		return fieldFloatObj;
	}
	public void setFloatObj( Float floatObj )
	{
		fieldFloatObj = floatObj;
	}
	
	// this is not an accessor, it's logic, so don't detect it as a column (there is no write method)
	public boolean isNotColumn()
	{
		return true;
	}
}
