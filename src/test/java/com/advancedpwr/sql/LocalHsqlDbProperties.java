package com.advancedpwr.sql;

import java.sql.Connection;

public class LocalHsqlDbProperties implements DatabaseConnectionProvider {

	public LocalHsqlDbProperties() {
		setUrl("jdbc:hsqldb:hsql://localhost:9001/xdb");
		setUsername("sa");
		setPassword("");
		setDriver("com.mysql.jdbc.Driver");
	}

	public Connection getConnection() { return null; }

	public void setUrl(String URL) {}

	public void setUsername(String userName) {}

	public void setPassword(String password) {}

	public void setDriver(String driver) {}
}
