/**
 * 
 */
package com.advancedpwr.sql;



/**
 * @author Matthew Avery, mavery@advancedpwr.com
 * CreatedStamped: Mar 30, 2012
 *
 */
public class CustomerDbClient extends DbClient<Customer>
{

	/* (non-Javadoc)
	 * @see com.advancedpwr.sql.DbClient#newInstance()
	 */
	protected Customer newInstance()
	{
		return new Customer();
	}

}
