package com.advancedpwr.sql;

import java.util.List;

import junit.framework.TestCase;

public class SqlAccessorTest extends TestCase
{

	public void testColumnDefs()
	{
		SqlAccessor acc = new SqlAccessor();
		
		
		acc.setData( new PrimitiveBean() );
		
		List<Column> cols = acc.columnDefs();
		StringBuffer sb = new StringBuffer();
		
		for ( Column column : cols )
		{
			sb.append( column.getName() + " " + column.getClass() + "\n" );
		}
		assertEquals( "BIGDECIMAL class com.advancedpwr.sql.BigDecimalColumn\n" + 
				"BIGINTEGER class com.advancedpwr.sql.BigIntegerColumn\n" + 
				"BOOLEAN class com.advancedpwr.sql.BooleanColumn\n" + 
				"INT class com.advancedpwr.sql.IntColumn\n" + 
				"INTEGEROBJ class com.advancedpwr.sql.IntegerColumn\n" + 
				"LONG class com.advancedpwr.sql.LongColumn\n" + 
				"LONGOBJ class com.advancedpwr.sql.LongColumn\n" + 
				"STRING class com.advancedpwr.sql.Column\n", sb.toString() );
	}

}
