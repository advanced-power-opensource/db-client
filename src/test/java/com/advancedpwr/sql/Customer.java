/**
 * 
 */
package com.advancedpwr.sql;




/**
 * @author Matthew Avery, mavery@advancedpwr.com
 * CreatedStamped: Mar 30, 2012
 *
 */
public class Customer extends DbObject
{
	
	protected boolean fieldGood;
	
	protected String fieldFirstName;
	
	protected String fieldLastName;

	public boolean isGood()
	{
		return fieldGood;
	}
	
	public boolean getGood()
	{
		return fieldGood;
	}
	
	
	public void setGood( boolean good )
	{
		fieldGood = good;
	}

	public String getFirstName()
	{
		return fieldFirstName;
	}

	public void setFirstName( String firstName )
	{
		fieldFirstName = firstName;
	}

	public String getLastName()
	{
		return fieldLastName;
	}

	public void setLastName( String lastName )
	{
		fieldLastName = lastName;
	}
	
}
