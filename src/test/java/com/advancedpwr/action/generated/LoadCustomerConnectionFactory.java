package com.advancedpwr.action.generated;

import static org.easymock.EasyMock.aryEq;
import static org.easymock.EasyMock.contains;
import static org.easymock.EasyMock.createNiceMock;
import static org.easymock.EasyMock.createStrictMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class LoadCustomerConnectionFactory
{

	protected Connection connection;

	public Connection buildConnection() throws SQLException
	{
		if ( connection != null ) 
		{
			return connection;
		}
		connection = createNiceMock( Connection.class );
		expect( connection.isClosed() ).andReturn( Boolean.FALSE );
		expect( connection.isClosed() ).andReturn( Boolean.FALSE );
		expect( connection.prepareStatement( contains("select * from CUSTOMER  WHERE ID = ?"),aryEq(buildStringArray_4_1() ) ) ).andReturn( buildPreparedStatement_2_1() );
		expect( connection.isClosed() ).andReturn( Boolean.FALSE );
		connection.close();
		replay( connection );
		return connection;
	}

	protected PreparedStatement preparedstatement_2_1;

	protected PreparedStatement buildPreparedStatement_2_1() throws SQLException
	{
		if ( preparedstatement_2_1 != null ) 
		{
			return preparedstatement_2_1;
		}
		preparedstatement_2_1 = createStrictMock( PreparedStatement.class );
		preparedstatement_2_1.setLong( new Integer( 1 ),new Long( 1l ) );
		expect( preparedstatement_2_1.executeQuery() ).andReturn( buildResultSet_9_2() );
		replay( preparedstatement_2_1 );
		return preparedstatement_2_1;
	}

	protected ResultSet resultset_9_2;

	protected ResultSet buildResultSet_9_2() throws SQLException
	{
		if ( resultset_9_2 != null ) 
		{
			return resultset_9_2;
		}
		resultset_9_2 = createStrictMock( ResultSet.class );
		expect( resultset_9_2.next() ).andReturn(  Boolean.TRUE );
		expect( resultset_9_2.getTimestamp( "CreatedDate".toUpperCase() ) ).andReturn( buildTimestamp_11_3() );
		
		expect( resultset_9_2.getObject( "FirstName".toUpperCase() ) ).andReturn( "James                                                                                               " );
		expect( resultset_9_2.getObject( "Good".toUpperCase() ) ).andReturn( new Integer( 1 ) );
		
		expect( resultset_9_2.getObject( "LastName".toUpperCase() ) ).andReturn( "Kirk                                                                                                " );
		expect( resultset_9_2.getTimestamp( "LastUpdated".toUpperCase() ) ).andReturn( null );
		expect( resultset_9_2.getLong( "ID" ) ).andReturn( new Long( "1" ) );
		replay( resultset_9_2 );
		return resultset_9_2;
	}

	protected Timestamp timestamp_11_3;

	protected Timestamp buildTimestamp_11_3()
	{
		timestamp_11_3 = new Timestamp( 1333080000000l );
		return timestamp_11_3;
	}

	protected String[] stringArray_4_1;

	protected String[] buildStringArray_4_1()
	{
		if ( stringArray_4_1 != null ) 
		{
			return stringArray_4_1;
		}
		stringArray_4_1 = new String[1];
		stringArray_4_1[0] = "ID";
		return stringArray_4_1;
	}
}
