package com.advancedpwr.action.generated;

import static org.easymock.EasyMock.aryEq;
import static org.easymock.EasyMock.contains;
import static org.easymock.EasyMock.createNiceMock;
import static org.easymock.EasyMock.createStrictMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;


public class LoadCustomerOrderByConnectionFactory
{

	protected Connection connection;

	public Connection buildConnection() throws SQLException
	{
		if ( connection != null ) 
		{
			return connection;
		}
		connection = createNiceMock( Connection.class );
		expect( connection.isClosed() ).andReturn( Boolean.FALSE );
		expect( connection.isClosed() ).andReturn( Boolean.FALSE );
		expect( connection.prepareStatement( contains("select * from CUSTOMER  WHERE FIRSTNAME = ? order by createddate desc"),aryEq(buildStringArray_4_1() ) ) ).andReturn( buildPreparedStatement_2_1() );
		expect( connection.isClosed() ).andReturn( Boolean.FALSE );
		connection.close();
		replay( connection );
		return connection;
	}

	protected PreparedStatement preparedstatement_2_1;

	protected PreparedStatement buildPreparedStatement_2_1() throws SQLException
	{
		if ( preparedstatement_2_1 != null ) 
		{
			return preparedstatement_2_1;
		}
		preparedstatement_2_1 = createStrictMock( PreparedStatement.class );
		preparedstatement_2_1.setString( new Integer( 1 ),"James" );
		expect( preparedstatement_2_1.executeQuery() ).andReturn( buildResultSet_9_2() );
		replay( preparedstatement_2_1 );
		return preparedstatement_2_1;
	}

	protected ResultSet resultset_9_2;

	protected ResultSet buildResultSet_9_2() throws SQLException
	{
		if ( resultset_9_2 != null ) 
		{
			return resultset_9_2;
		}
		resultset_9_2 = createStrictMock( ResultSet.class );
		expect( resultset_9_2.next() ).andReturn(  Boolean.TRUE );
		expect( resultset_9_2.getTimestamp( "CREATEDDATE" ) ).andReturn( buildTimestamp_11_3() );
		
		expect( resultset_9_2.getObject( "FIRSTNAME" ) ).andReturn( "James                                                                                               " );
		expect( resultset_9_2.getObject( "GOOD" ) ).andReturn( new Integer( 1 ) );
		
		expect( resultset_9_2.getObject( "LASTNAME" ) ).andReturn( "Kirk                                                                                                " );
		expect( resultset_9_2.getTimestamp( "LASTUPDATED" ) ).andReturn( null );
		expect( resultset_9_2.getLong( "ID" ) ).andReturn( new Long( "1" ) );
		replay( resultset_9_2 );
		return resultset_9_2;
	}

	protected Timestamp timestamp_11_3;

	protected Timestamp buildTimestamp_11_3()
	{
		timestamp_11_3 = new Timestamp( 1333080000000l );
		return timestamp_11_3;
	}

	protected String[] stringArray_4_1;

	protected String[] buildStringArray_4_1()
	{
		if ( stringArray_4_1 != null ) 
		{
			return stringArray_4_1;
		}
		stringArray_4_1 = new String[1];
		stringArray_4_1[0] = "ID";
		return stringArray_4_1;
	}
}
