package com.advancedpwr.action.generated;

import static org.easymock.EasyMock.aryEq;
import static org.easymock.EasyMock.contains;
import static org.easymock.EasyMock.createNiceMock;
import static org.easymock.EasyMock.createStrictMock;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.easymock.EasyMock.replay;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;


public class InsertCustomerConnectionFactory
{

	protected Connection connection;

	public Connection buildConnection() throws SQLException
	{
		if ( connection != null ) 
		{
			return connection;
		}
		connection = createNiceMock( Connection.class );
		expect( connection.isClosed() ).andReturn( Boolean.FALSE );
		expect( connection.isClosed() ).andReturn( Boolean.FALSE );
		expect( connection.prepareStatement( contains("INSERT INTO CUSTOMER (CREATEDDATE, FIRSTNAME, GOOD, LASTNAME, LASTUPDATED ) values ( ?, ?, ?, ?, ?)"),aryEq( buildStringArray_4_1() ) ) ).andReturn( buildPreparedStatement_2_1() );
		expect( connection.isClosed() ).andReturn( Boolean.FALSE );
		connection.close();
		replay( connection );
		return connection;
	}

	protected PreparedStatement preparedstatement_2_1;

	protected PreparedStatement buildPreparedStatement_2_1() throws SQLException
	{
		if ( preparedstatement_2_1 != null ) 
		{
			return preparedstatement_2_1;
		}
		preparedstatement_2_1 = createStrictMock( PreparedStatement.class );
		preparedstatement_2_1.setTimestamp( eq( 1 ), isA( Timestamp.class ) );
		
		preparedstatement_2_1.setString( new Integer( 2 ),"James" );
		preparedstatement_2_1.setInt( new Integer( 3 ),new Integer( 1 ) );
		preparedstatement_2_1.setString( new Integer( 4 ),"Kirk" );
		preparedstatement_2_1.setTimestamp( eq( 5 ), isA( Timestamp.class ) );
		expect( preparedstatement_2_1.executeUpdate() ).andReturn( new Integer( 1 ) );
		expect( preparedstatement_2_1.getGeneratedKeys() ).andReturn( buildResultSet_15_2() );
		preparedstatement_2_1.close();
		replay( preparedstatement_2_1 );
		return preparedstatement_2_1;
	}

	protected Timestamp timestamp_6_1;

	protected Timestamp buildTimestamp_6_1()
	{
		timestamp_6_1 = new Timestamp( 1333133935451l );
		return timestamp_6_1;
	}

	protected ResultSet resultset_15_2;

	protected ResultSet buildResultSet_15_2() throws SQLException
	{
		if ( resultset_15_2 != null ) 
		{
			return resultset_15_2;
		}
		resultset_15_2 = createStrictMock( ResultSet.class );
		expect( resultset_15_2.next() ).andReturn(  Boolean.TRUE );
		expect( resultset_15_2.getLong( new Integer( 1 ) ) ).andReturn( new Long( 1l ) );
		resultset_15_2.close();
		replay( resultset_15_2 );
		return resultset_15_2;
	}

	protected String[] stringArray_4_1;

	protected String[] buildStringArray_4_1()
	{
		if ( stringArray_4_1 != null ) 
		{
			return stringArray_4_1;
		}
		stringArray_4_1 = new String[1];
		stringArray_4_1[0] = "ID";
		return stringArray_4_1;
	}
}
