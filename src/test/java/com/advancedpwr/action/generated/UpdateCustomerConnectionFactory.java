package com.advancedpwr.action.generated;

import static org.easymock.EasyMock.aryEq;
import static org.easymock.EasyMock.contains;
import static org.easymock.EasyMock.createNiceMock;
import static org.easymock.EasyMock.createStrictMock;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.easymock.EasyMock.replay;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

public class UpdateCustomerConnectionFactory 
{

	protected Connection connection;

	public Connection buildConnection() throws SQLException
	{
		if ( connection != null ) 
		{
			return connection;
		}
		connection = createNiceMock( Connection.class );
		expect( connection.isClosed() ).andReturn( Boolean.FALSE );
		expect( connection.prepareStatement( contains("UPDATE CUSTOMER SET CREATEDDATE = ?, FIRSTNAME = ?, GOOD = ?, LASTNAME = ?, LASTUPDATED = ? WHERE ID = ?"),aryEq(buildStringArray_4_1() ) ) ).andReturn( buildPreparedStatement_2_1() );
		expect( connection.isClosed() ).andReturn( Boolean.FALSE );
		connection.close();
		replay( connection );
		return connection;
	}

	protected PreparedStatement preparedstatement_2_1;

	protected PreparedStatement buildPreparedStatement_2_1() throws SQLException
	{
		if ( preparedstatement_2_1 != null ) 
		{
			return preparedstatement_2_1;
		}
		preparedstatement_2_1 = createStrictMock( PreparedStatement.class );
		int i = 1;
		preparedstatement_2_1.setTimestamp( eq( i++ ), isA( Timestamp.class ) );
		
		preparedstatement_2_1.setString( new Integer( i++ ),"Tiberius" );
		preparedstatement_2_1.setInt( new Integer( i++ ),new Integer( 0 ) );
		preparedstatement_2_1.setString( new Integer( i++ ),"Kirk" );
		preparedstatement_2_1.setTimestamp( eq( i++ ), isA( Timestamp.class ) );
		preparedstatement_2_1.setObject( new Integer( i++ ),new Long( 1l ) );
		expect( preparedstatement_2_1.execute() ).andReturn( Boolean.FALSE );
		replay( preparedstatement_2_1 );
		return preparedstatement_2_1;
	}

	protected Timestamp timestamp_6_1;

	protected Timestamp buildTimestamp_6_1()
	{
		timestamp_6_1 = new Timestamp( 1333136937857l );
		return timestamp_6_1;
	}

	protected String[] stringArray_4_1;

	protected String[] buildStringArray_4_1()
	{
		if ( stringArray_4_1 != null ) 
		{
			return stringArray_4_1;
		}
		stringArray_4_1 = new String[1];
		stringArray_4_1[0] = "ID";
		return stringArray_4_1;
	}
}
