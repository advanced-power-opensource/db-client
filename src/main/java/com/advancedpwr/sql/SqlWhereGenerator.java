/**
 * 
 */
package com.advancedpwr.sql;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * @author Matthew Avery, mavery@advancedpwr.com
 * Created: Oct 12, 2011
 *
 */
public class SqlWhereGenerator extends SqlGenerator
{
	public String toWhereClause()
	{
		StringBuffer sb = new StringBuffer();
		
		List<Column> cols = populatedColumns();
		boolean first = true;
		if ( cols.size() > 0 )
		{
			sb.append( " WHERE " );
		}
		for ( Iterator iterator = cols.iterator(); iterator.hasNext(); )
		{

			Column column = (Column) iterator.next();
			if ( !first )
			{
				sb.append( " AND " );
			}
			sb.append( column.getName() + " = ?" );
			first = false;
		}
		return sb.toString();
	}
	
	public String toWhereClauseMessage()
	{
		StringBuffer sb = new StringBuffer();
		sb.append( " WHERE " );
		List<Column> cols = populatedColumns();
		boolean first = true;
		for ( Iterator iterator = cols.iterator(); iterator.hasNext(); )
		{

			Column column = (Column) iterator.next();
			if ( !first )
			{
				sb.append( " AND " );
			}
			sb.append( column.getName() + " = '" + column.getValue() + "'");
			first = false;
		}
		return sb.toString();
	}

	protected List<Column> populatedColumns()
	{
		List<Column> all = super.populatedColumns();
		List<Column> columnsWithValues = new ArrayList<Column>();
		for ( Column column : all )
		{
			if ( column.hasValue() )
			{
				columnsWithValues.add( column );
			}
		}
		if ( hasPrimaryKey() && primaryKey().getValue() != null )
		{
			columnsWithValues.add( primaryKey() );
		}
		return columnsWithValues;
	}
}
