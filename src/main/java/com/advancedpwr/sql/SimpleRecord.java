/**
 * 
 */
package com.advancedpwr.sql;

/**
 * @author Matthew Avery, mavery@advancedpwr.com
 * Created: Jun 29, 2012
 *
 */
public class SimpleRecord implements DbRecord
{

	/**
	 * 
	 */
	public static final String WILDCARD = "*";

	/* (non-Javadoc)
	 * @see com.advancedpwr.sql.DbRecord#primaryKey()
	 */
	public Column primaryKey()
	{
		return null;
	}

	/* (non-Javadoc)
	 * @see com.advancedpwr.sql.DbRecord#tableName()
	 */
	public String tableName()
	{
		return getClass().getSimpleName().toUpperCase();
	}

	protected String asWildcard( String s )
	{
		if ( s == null || s.length() == 0 )
		{
			//Oracle treats a zero length String as null, but we should not
			return WILDCARD;
		}
		return s;
	}

}
