package com.advancedpwr.sql;

import java.util.Date;

public class DbObject implements DbRecord, DateStamped, CreatedStamped
{
	protected IdColumn fieldKey;
	
	protected Date fieldLastUpdated;
	
	protected Date fieldCreatedDate;



	/* (non-Javadoc)
	 * @see com.advancedpwr.sql.Created#getCreatedDate()
	 */
	@Override
	public Date getCreatedDate()
	{
		return fieldCreatedDate;
	}

	/* (non-Javadoc)
	 * @see com.advancedpwr.sql.Created#setCreatedDate(java.util.Date)
	 */
	@Override
	public void setCreatedDate( Date createdDate )
	{
		fieldCreatedDate = createdDate;
	}

	/* (non-Javadoc)
	 * @see com.advancedpwr.sql.DbRecord#primaryKey()
	 */
	public IdColumn primaryKey()
	{
		if ( fieldKey == null )
		{
			fieldKey = new IdColumn();
		}
		return fieldKey;
	}

	/* (non-Javadoc)
	 * @see com.advancedpwr.sql.DbRecord#tableName()
	 */
	public String tableName()
	{
		return getClass().getSimpleName().toUpperCase();
	}
	
	public void setId( long inId )
	{
		primaryKey().setValue( inId );
	}

	public long id()
	{
		if ( primaryKey().getValue() == null )
		{
			return 0;
		}
		return primaryKey().getValue();
	}

	public Date getLastUpdated()
	{
		return fieldLastUpdated;
	}

	public void setLastUpdated( Date lastUpdated )
	{
		fieldLastUpdated = lastUpdated;
	}
}
