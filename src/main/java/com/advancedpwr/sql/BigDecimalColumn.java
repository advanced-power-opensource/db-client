package com.advancedpwr.sql;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BigDecimalColumn extends Column<BigDecimal>
{
	public void populate( int index, PreparedStatement stmt ) throws SQLException
	{
		stmt.setBigDecimal( index, getValue() );
		debugPopulate( index );
	}
	
	protected void debugPopulate( int index )
	{
		LOGGER.debug( "stmt.setBigDecimal( " + index +
				", " + getValue() +  
				" )" );
	}

	
	public String type()
	{
		return "DECIMAL";
	}

	public BigDecimal mapValue( ResultSet rs ) throws SQLException
	{
		return rs.getBigDecimal( getName() );
	}
	
	
}
