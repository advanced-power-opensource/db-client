package com.advancedpwr.sql;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BigIntegerColumn extends Column<BigInteger>
{
	public void populate( int index, PreparedStatement stmt ) throws SQLException
	{
		if ( getValue() == null )
		{
			stmt.setBigDecimal( index, null );
		}
		else
		{
			stmt.setBigDecimal( index, new BigDecimal( getValue() ) );
		}
		debugPopulate( index );
	}
	
	protected void debugPopulate( int index )
	{
		LOGGER.debug( "stmt.setBigInteger( " + index +
				", " + getValue() +  
				" )" );
	}

	
	public String type()
	{
		return "BIGINT";
	}

	public BigInteger mapValue( ResultSet rs ) throws SQLException
	{
		BigDecimal val = rs.getBigDecimal( getName() );
		if ( val != null )
		{
			return val.toBigInteger();
		}
		return null;
	}
	
	
}
