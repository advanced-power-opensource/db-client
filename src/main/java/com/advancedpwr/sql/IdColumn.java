/**
 * 
 */
package com.advancedpwr.sql;

/**
 * @author Matthew Avery, mavery@advancedpwr.com
 * Created: Jun 29, 2012
 *
 */
public class IdColumn extends LongColumn
{
	public IdColumn()
	{
		setName( "ID" );
	}
}
