/**
 * 
 */
package com.advancedpwr.sql;

import java.util.Date;

/**
 * @author Matthew Avery, mavery@advancedpwr.com
 * Created: Jun 29, 2012
 *
 */
public interface DateStamped
{
	public Date getLastUpdated();

	public void setLastUpdated( Date date );
}
