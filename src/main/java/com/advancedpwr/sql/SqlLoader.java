package com.advancedpwr.sql;

import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.List;

public class SqlLoader extends SqlAccessor
{
	protected ResultSet fieldResultSet;
	protected Column fieldColumn;
	
	public void populate()
	{
		try
		{
			populateRaw();
		}
		catch ( Exception e )
		{
			throw new RuntimeException(e);
		}
	}
	
	protected void populateRaw() throws Exception
	{
		List<Column> cols = columnDefs();
		for ( Iterator iterator = cols.iterator(); iterator.hasNext(); )
		{
			Column column = (Column) iterator.next();
			setColumn( column );
			
			invokeMethod();
		}
		if ( hasPrimaryKey() )
		{
			primaryKey().setValue( primaryKey().mapValue( getResultSet() ) );
		}
	}

	
	protected void invokeMethod() throws Exception
	{			
		Method method = getColumn().getWriteMethod();
		method.invoke( getData(), new Object[]{ getColumn().mapValue( getResultSet() ) } );
	}

	public ResultSet getResultSet()
	{
		return fieldResultSet;
	}

	public void setResultSet( ResultSet resultSet )
	{
		fieldResultSet = resultSet;
	}

	protected boolean isGetter( String inName )
	{
		return inName.startsWith( "get" ) && !inName.equals( "getClass" );
	}

	public Column getColumn()
	{
		return fieldColumn;
	}

	public void setColumn( Column currentColumn )
	{
		fieldColumn = currentColumn;
	}
}
