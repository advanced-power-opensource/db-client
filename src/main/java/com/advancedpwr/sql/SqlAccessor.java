package com.advancedpwr.sql;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.List;

public class SqlAccessor
{
	protected DbRecord fieldData;

	protected Class interfaceClass()
	{
		return getData().getClass();
	}

	public DbRecord getData()
	{
		return fieldData;
	}

	public void setData( DbRecord data )
	{
		fieldData = data;
	}
	
	protected Column primaryKey()
	{
		return getData().primaryKey();
	}
	
	protected boolean hasPrimaryKey()
	{
		return primaryKey() != null;
	}
	
	protected List<Column> columnDefs()
	{
		return columns( new ColumnOperation()
		{
			public void exec( Column column )
			{
			}
		});
	}
	
	protected List<Column> columns( ColumnOperation op )
	{
		PropertyDescriptor[] descriptors = propertyDescriptors();
		
		List<Column> columns = new ArrayList<Column>();
		for ( int i = 0; i < descriptors.length; i++ )
		{
			PropertyDescriptor descriptor = descriptors[i];
			
			if ( isAccessor( descriptor ) )
			{
				Column column = Column.create( descriptor );
				if ( column == null )
				{
					continue;
				}
				column.setName( descriptor.getName().toUpperCase() );
				op.exec( column );
				columns.add( column );	
			}
		}
		
		return columns;
	}

	protected PropertyDescriptor[] propertyDescriptors()
	{
		try
		{
			BeanInfo info = Introspector.getBeanInfo( interfaceClass() );
			PropertyDescriptor[] descriptors = info.getPropertyDescriptors();
			return descriptors;
		}
		catch ( Exception e )
		{
			throw new RuntimeException( e );
		}
	}

	protected boolean isAccessor( PropertyDescriptor desc )
	{
		return desc.getReadMethod() != null && desc.getWriteMethod() != null;
	}

}
