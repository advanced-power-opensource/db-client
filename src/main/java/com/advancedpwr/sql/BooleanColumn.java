package com.advancedpwr.sql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BooleanColumn extends Column
{
	public void populate( int index, PreparedStatement stmt ) throws SQLException
	{
		// sane database flavor
//		stmt.setBoolean( index, (Boolean)getValue() );
		// Oracle flavor
		stmt.setInt( index, intValue() );
		debugPopulate( index );
	}
	
	protected void debugPopulate( int index )
	{
		LOGGER.debug( "stmt.setInt( " + index + 
				", " + intValue() +
				" )" );
	}

	// going in to the db
	protected int intValue()
	{
		int i = 0;
		if ( (Boolean)getValue() )
		{
			i = 1;
		}
		return i;
	}
	
	public String type()
	{
		return "INTEGER";
	}

	// coming out of the db
	public Object mapValue( ResultSet rs ) throws SQLException
	{
		Number num = (Number)super.mapValue( rs );
		if ( num.intValue() > 0 )
		{
			return true;
		}
		return false;
	}
	
	public boolean hasValue()
	{
		return getValue() != null && ((Boolean)getValue());
	}
}
