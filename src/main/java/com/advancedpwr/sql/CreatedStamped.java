package com.advancedpwr.sql;

import java.util.Date;

public interface CreatedStamped
{

	public abstract Date getCreatedDate();

	public abstract void setCreatedDate( Date createdDate );

}