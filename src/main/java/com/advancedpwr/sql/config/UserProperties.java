package com.advancedpwr.sql.config;

import java.util.Properties;

public class UserProperties extends AbstractProperties
{
	public static String USERNAME="username";
	public static String PASSWORD="password";
	
	protected Crypt fieldCrypt;
	
	public UserProperties()
	{
		super();
	}

	public UserProperties( Properties defaults )
	{
		super( defaults );
	}
	
	public String getUsername()
	{
		return getProperty( USERNAME );
	}
	
	public void setUsername( String username )
	{
		setProperty( USERNAME, username );
	}
	
	public String getPassword()
	{
		return decrypt( getProperty( PASSWORD ) );
	}
    
	public void setPassword( String password )
	{
		setProperty( PASSWORD, password );
	}
	
    protected String decrypt( String inString )
    {
    	if ( getCrypt() == null )
    	{
    		return inString;
    	}
    	return getCrypt().decrypt( inString );
    }

	public Crypt getCrypt()
	{
		return fieldCrypt;
	}

	public void setCrypt( Crypt Crypt )
	{
		fieldCrypt = Crypt;
	}
    
    
 
}
