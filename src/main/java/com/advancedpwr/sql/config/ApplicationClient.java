/**
 * 
 */
package com.advancedpwr.sql.config;

import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import com.advancedpwr.sql.DbClient;

/**
 * @author Matthew Avery, mavery@advancedpwr.com
 * Created: Nov 26, 2012
 *
 */
public class ApplicationClient extends DbClient<Application>
{

	/* (non-Javadoc)
	 * @see com.advancedpwr.sql.DbClient#newInstance()
	 */
	protected Application newInstance()
	{
		return new Application();
	}
	
	public Properties load( String inName ) throws SQLException
	{
		holdConnection();
		try
		{
			// Load general defaults defaults
			Application template = new Application();
			Properties props = new Properties();
			populatePropertiesByTemplate( template, props );
			
			// Override general defaults with server specific variants
			template.setServer( SystemConnection.serverName() );
			populatePropertiesByTemplate( template, props );
			
			// Override defaults with properties specific to this application
			template.setServer( null );
			template.setName( inName );
			populatePropertiesByTemplate( template, props );
			
			// Override application specific properties with server specific variants
			template.setServer( SystemConnection.serverName() );
			populatePropertiesByTemplate( template, props );
			return props;
		}
		finally
		{
			releaseConnection();
			closeConnection();
		}

	}

	protected void populatePropertiesByTemplate( Application template, Properties props )
			throws SQLException
	{
		List<Application> appProperties = loadAllWhere( template );
		for ( Application applicationProperty : appProperties )
		{
			props.setProperty( applicationProperty.getPropertyKey(), applicationProperty.getPropertyValue() );
		}
	}

	
}
