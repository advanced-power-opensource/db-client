/**
 * 
 */
package com.advancedpwr.sql.config;

import java.util.Properties;

/**
 * @author Matthew Avery, mavery@advancedpwr.com
 * Created: Oct 7, 2011
 *
 */
public abstract class AbstractProperties extends Properties
{
	protected String fieldPrefix;
	protected String fieldSuffix;
	
	public AbstractProperties()
	{
		super();
	}

	public AbstractProperties( Properties properties )
	{
		super( properties );
	}
	
	public String getProperty( String key )
	{
		return super.getProperty( wrap( key ));
	}

	protected String wrap( String key )
	{
		return getPrefix() + key + getSuffix();
	}

	public Object setProperty( String key, String value )
	{
		return super.setProperty( wrap( key ), value );
	}
	
    public static Properties load( String basename )
    {
    	try
		{
    		return new PropertyLoader().loadFromFile( basename );
		}
		catch ( Exception e )
		{
			throw new RuntimeException(e);
		}
    }
  
    public Properties load()
    {
    	return load( getClass().getName().replaceFirst( "Properties", "") );
    }  
    
    


	public String getPrefix()
	{
		if ( fieldPrefix == null )
		{
			fieldPrefix = "";
		}
		return fieldPrefix;
	}

	public void setPrefix( String prefix )
	{
		fieldPrefix = prefix;
	}

	public String getSuffix()
	{
		if ( fieldSuffix == null )
		{
			fieldSuffix = "";
		}
		return fieldSuffix;
	}

	public void setSuffix( String suffix )
	{
		fieldSuffix = suffix;
	}


}
