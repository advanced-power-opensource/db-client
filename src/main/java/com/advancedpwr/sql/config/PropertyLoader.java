/**
 * 
 */
package com.advancedpwr.sql.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author Matthew Avery, mavery@advancedpwr.com
 * Created: Nov 27, 2012
 *
 */
public class PropertyLoader
{	
    public Properties loadFromFile( String basename ) throws IOException
    {
    		File file = new File( SystemConnection.configDirectory(), basename + ".properties");
    		Properties props = new Properties();
    		FileInputStream fis = new FileInputStream( file );
    		props.load( fis );
    		fis.close();
    		return props;
    }
    
    public Properties loadFromDatabase( DatabaseProperties dbProperties, String basename ) throws SQLException
    {
    	ApplicationClient client = new ApplicationClient();
    	client.setProperties( dbProperties );
    	return client.load( basename );
    }
    
}
