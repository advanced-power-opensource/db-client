package com.advancedpwr.sql.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.advancedpwr.sql.DatabaseConnectionProvider;

public class DatabaseProperties extends AbstractProperties implements DatabaseConnectionProvider
{
	public static String PREFIX= "db.";
	public static String URL = "url";
	public static String DATA_SOURCE="datasource";
	public static String DRIVER = "driver";
	
	protected UserProperties fieldUserProperties;
	
	public DatabaseProperties()
	{
		setPrefix( PREFIX );
	}

	public DatabaseProperties( Properties defaults )
	{
		super( defaults );
		setPrefix( PREFIX );
	}
	
	public UserProperties getUserProperties()
	{
		if ( fieldUserProperties == null )
		{
			fieldUserProperties = createUserProperties();
		}
		return fieldUserProperties;
	}
	
	protected UserProperties createUserProperties()
	{
		UserProperties props = new UserProperties( this );
		return props;
	}
	
	
	public String getUrl()
	{
		return getProperty( URL );
	}
	
	public void setUrl( String url )
	{
		setProperty( URL, url );
	}
	
	public String getDataSourceName()
	{
		return getProperty( DATA_SOURCE );
	}
	
	public String getDriver()
	{
		return getProperty( DRIVER );
	}
	
	public void setDriver( String driver )
	{
		setProperty( DRIVER, driver );
	}
	
	protected DataSource lookupDataSource()
	{
		try
		{
			return (DataSource) createInitialContext().lookup( getDataSourceName() );
		}
		catch ( NamingException e )
		{
			try
			{
				Context envContext = (Context) createInitialContext().lookup("java:comp/env");
				return (DataSource) envContext.lookup(getDataSourceName());
			}
			catch ( NamingException e1 )
			{
				throw new RuntimeException( e1 );
			}
		}
	}

	protected InitialContext createInitialContext() throws NamingException
	{
		return new InitialContext();
	}

	public Connection getConnection() throws SQLException
	{
		if ( hasDataSource() )
		{
			return lookupDataSource().getConnection();
		}
		try
		{
			Class.forName( getDriver() );
		}
		catch ( ClassNotFoundException e )
		{
			throw new RuntimeException( e );
		}
        return DriverManager.getConnection(getUrl(),getUsername(),getPassword());
	}

	protected boolean hasDataSource()
	{
		return getDataSourceName() != null;
	}

	public String getUsername()
	{
		return getUserProperties().getUsername();
	}

	public void setUsername( String username )
	{
		getUserProperties().setUsername( username );
	}

	public String getPassword()
	{
		return getUserProperties().getPassword();
	}

	public void setPassword( String password )
	{
		getUserProperties().setPassword( password );
	}
}
