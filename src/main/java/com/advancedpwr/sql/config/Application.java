/**
 * 
 */
package com.advancedpwr.sql.config;

import com.advancedpwr.sql.SimpleRecord;

/**
 * @author Matthew Avery, mavery@advancedpwr.com
 * Created: Nov 26, 2012
 *
 */
public class Application extends SimpleRecord
{
	protected String fieldName;
	protected String fieldPropertyKey;
	protected String fieldPropertyValue;
	protected String fieldServer;
	
	public String getName()
	{
		return asWildcard( fieldName );
	}
	public void setName( String name )
	{
		fieldName = name;
	}
	public String getPropertyKey()
	{
		return fieldPropertyKey;
	}
	public void setPropertyKey( String propertyKey )
	{
		fieldPropertyKey = propertyKey;
	}
	public String getPropertyValue()
	{
		return fieldPropertyValue;
	}
	public void setPropertyValue( String propertyValue )
	{
		fieldPropertyValue = propertyValue;
	}
	public String getServer()
	{
		return asWildcard( fieldServer );
	}
	public void setServer( String server )
	{
		fieldServer = server;
	}
	


	
}
