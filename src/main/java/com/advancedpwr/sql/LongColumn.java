package com.advancedpwr.sql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LongColumn extends Column<Long>
{
	public void populate( int index, PreparedStatement stmt ) throws SQLException
	{
		stmt.setLong( index, getValue() );
		debugPopulate( index );
	}
	
	protected void debugPopulate( int index )
	{
		LOGGER.debug( "stmt.setLong( " + index +
				", " + getValue() +  
				" )" );
	}

	
	public String type()
	{
		return "INTEGER";
	}

	public Long mapValue( ResultSet rs ) throws SQLException
	{
		return rs.getLong( getName() );
	}
	
	public boolean hasValue()
	{
		return getValue() != null && getValue().longValue() > 0;
	}
}
