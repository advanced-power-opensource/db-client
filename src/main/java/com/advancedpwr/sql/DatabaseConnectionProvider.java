package com.advancedpwr.sql;

import java.sql.Connection;
import java.sql.SQLException;

public interface DatabaseConnectionProvider {

	Connection getConnection() throws SQLException;
}
