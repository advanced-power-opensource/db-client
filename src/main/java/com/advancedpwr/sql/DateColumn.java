package com.advancedpwr.sql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

public class DateColumn extends Column<Date>
{
	public void populate( int index, PreparedStatement stmt ) throws SQLException
	{
		Date date = getValue();
		if ( date == null )
		{
			stmt.setTimestamp( index, null );
		}
		else
		{
			stmt.setTimestamp( index, new Timestamp( date.getTime() ) );
		}
		debugPopulate( index );
	}
	
	protected void debugPopulate( int index )
	{
		Date date = getValue();
		if ( date == null )
		{
			LOGGER.debug( "stmt.setTimestamp( " + index +
					", null ) " );
		}
		else
		{
		LOGGER.debug( "stmt.setTimestamp( " + index +
				", new Timestamp( " + date.getTime() +  
				" ) )" );
		}
	}
	
	public String type()
	{
		return "TIMESTAMP";
	}

	public Date mapValue( ResultSet rs ) throws SQLException
	{
		return rs.getTimestamp( getName() );
	}
	
	
}
