package com.advancedpwr.sql;

import java.util.ArrayList;
import java.util.Iterator;

public class SqlList extends ArrayList<String>
{
	public String toString()
	{
		StringBuilder b = new StringBuilder();
		b.append( "(" );
		for ( Iterator iterator = iterator(); iterator.hasNext(); )
		{
			String value = (String) iterator.next();
			b.append( "'" );
			b.append( value );
			b.append( "'" );
			if( iterator.hasNext() )
			{
				b.append(", ");
			}
		}
		b.append( ")");
		return b.toString();
	}
}
