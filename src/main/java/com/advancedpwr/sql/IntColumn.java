package com.advancedpwr.sql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class IntColumn extends Column<Integer>
{
	public void populate( int index, PreparedStatement stmt ) throws SQLException
	{
		Integer value = getValue();
		if ( value == null )
		{
			value = 0;
		}
		stmt.setInt( index, value );
		debugPopulate( index );
		
	}
	
	protected void debugPopulate( int index )
	{
		LOGGER.debug( "stmt.setInt( " + index +
				", " + getValue() +  
				" )" );
	}

	
	public String type()
	{
		return "INTEGER";
	}

	public Integer mapValue( ResultSet rs ) throws SQLException
	{
		return rs.getInt( getName() );
	}
	
	public boolean hasValue()
	{
		return getValue() != null && getValue().intValue() > 0;
	}
}
