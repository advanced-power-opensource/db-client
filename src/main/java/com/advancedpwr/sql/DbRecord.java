/**
 * 
 */
package com.advancedpwr.sql;

/**
 * @author Matthew Avery, mavery@advancedpwr.com
 * Created: Jun 29, 2012
 *
 */
public interface DbRecord
{
	/*
	 * It's OK for this to return null.
	 */
	public Column primaryKey();
	
	public String tableName();
}
