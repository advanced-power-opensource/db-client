package com.advancedpwr.sql;


public interface ColumnOperation
{

	void exec( Column column );

}
