package com.advancedpwr.sql;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Column<T>
{
	protected static final Logger LOGGER = LoggerFactory.getLogger( Column.class );
	
	//TODO:  Store byte array as a base 64 encoded string
	public static Column create( PropertyDescriptor inDescriptor )
	{
		Class inClass = inDescriptor.getPropertyType();
		if ( inClass.equals( String.class ) )
		{
			return new Column().setDescriptor( inDescriptor );
		}
		if ( inClass.equals( Date.class ) )
		{
			return new DateColumn().setDescriptor( inDescriptor );
		}
		if ( inClass.equals( long.class ) || inClass.equals( Long.class ) )
		{
			return new LongColumn().setDescriptor( inDescriptor );
		}
		if ( inClass.equals( int.class ) )
		{
			return new IntColumn().setDescriptor( inDescriptor );
		}
		if ( inClass.equals( Integer.class ) )
		{
			return new IntegerColumn().setDescriptor( inDescriptor );
		}
		if ( inClass.equals( boolean.class ) )
		{
			return new BooleanColumn().setDescriptor( inDescriptor );
		}
		if ( inClass.equals( BigDecimal.class ) )
		{
			return new BigDecimalColumn().setDescriptor( inDescriptor );
		}
		if ( inClass.equals( BigInteger.class ) )
		{
			return new BigIntegerColumn().setDescriptor( inDescriptor );
		}
		
		
		return null;
	}
	public String fieldName;
	public T fieldValue;
	
	protected PropertyDescriptor fieldDescriptor;
	
	public String getName()
	{
		return fieldName;
	}
	public void setName( String name )
	{
		fieldName = name;
	}
	public T getValue()
	{
		return fieldValue;
	}
	public void setValue( T value )
	{
		fieldValue = value;
	}
	
	public void populate( int index, PreparedStatement stmt ) throws SQLException
	{
		String value = (String)getValue();
		if ( value != null )
		{
			value = value.trim();
		}
		stmt.setString( index, value );
		debugPopulate( index );
	}

	protected void debugPopulate( int index )
	{
		LOGGER.debug( "stmt.setString( " + index +
				", \"" + getValue() +  
				"\" )" );
	}
	
	//This method is for generating hsql table creation sql
	public String type()
	{
		return "VARCHAR(100)";
	}

	public T mapValue( ResultSet rs ) throws SQLException
	{
		T value;
		try
		{
			value = (T)rs.getObject( getName() );
		}
		catch ( SQLException e )
		{
			if ( e.getMessage().startsWith( "Invalid column" ) )
			{
				return null;
			}
			throw e;
		}
		if ( value instanceof String )
		{
			value = (T)( (String) value ).trim();
		}
		return value;
	}
	
	public boolean hasValue()
	{
		return getValue() != null;
	}
	protected PropertyDescriptor getDescriptor()
	{
		return fieldDescriptor;
	}
	protected Column setDescriptor( PropertyDescriptor inDescriptor )
	{
		fieldDescriptor = inDescriptor;
		return this;
	}
	public Method getWriteMethod()
	{
		return getDescriptor().getWriteMethod();
	}
	public Method getReadMethod()
	{
		return getDescriptor().getReadMethod();
	}
}
