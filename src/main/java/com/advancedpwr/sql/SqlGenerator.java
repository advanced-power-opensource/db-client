package com.advancedpwr.sql;

import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SqlGenerator extends SqlAccessor
{
	private static final Logger LOGGER = LoggerFactory.getLogger( SqlGenerator.class );

	public String getTableName()
	{
		return getData().tableName();
	}

	public String toInsert()
	{
		StringBuffer sb = new StringBuffer();
		sb.append( "INSERT INTO " );
		sb.append( getTableName() );
		sb.append( " (" );
		List<Column> cols = columnDefs();
		int count = 0;
		for ( Iterator iterator = cols.iterator(); iterator.hasNext(); )
		{
			Column column = (Column) iterator.next();
			sb.append( column.getName() );
			if ( iterator.hasNext() )
			{
				sb.append( ", " );
			}
			count++;
		}
		sb.append( " ) values ( " );
		for ( int i = 0; i < count; i++ )
		{
			sb.append( "?" );
			if ( i < ( count - 1 ) )
			{
				sb.append( ", " );
			}
		}
		sb.append( ")" );
		LOGGER.debug( sb.toString() );
		return sb.toString();
	}

	public String toCreateTable()
	{
		StringBuffer sb = new StringBuffer();
		sb.append( "CREATE TABLE " );
		sb.append( getTableName() );
		sb.append( " (\n" );
		if ( primaryKey() != null )
		{
			sb.append( primaryKey().getName() );
			sb.append( " IDENTITY,\n" );
		}
		List<Column> cols = columnDefs();
		for ( Iterator iterator = cols.iterator(); iterator.hasNext(); )
		{
			Column column = (Column) iterator.next();
			//			if ( column == null )
			//			{
			//				continue;
			//			}
			sb.append( column.getName().toUpperCase() );
			sb.append( " " + column.type() );
			if ( iterator.hasNext() )
			{
				sb.append( ",\n" );
			}
		}
		sb.append( " )" );
		return sb.toString();
	}

	public String toUpdate()
	{
		StringBuffer sb = new StringBuffer();
		sb.append( "UPDATE " + getTableName() + " SET " );
		List<Column> cols = populatedColumns();
		for ( Iterator iterator = cols.iterator(); iterator.hasNext(); )
		{
			Column column = (Column) iterator.next();
			sb.append( column.getName() + " = ?" );
			if ( iterator.hasNext() )
			{
				sb.append( ", " );
			}
		}
		LOGGER.debug( sb.toString() );
		return sb.toString();
	}

	protected List<Column> populatedColumns()
	{
		return columns( new ColumnOperation()
		{
			
			public void exec( Column column )
			{
				column.setValue( invokeMethod( column.getReadMethod() ) );
			}
		} );
	}

	protected Object invokeMethod( Method method )
	{
		try
		{
			return invokeMethodRaw( method );
		}
		catch ( Exception e )
		{
			throw new RuntimeException( e );
		}
	}

	protected Object invokeMethodRaw( Method method ) throws Exception
	{
		return method.invoke( getData(), new Object[] {} );
	}

	public int populateStatement( PreparedStatement stmt ) throws SQLException
	{
		return populateStatement( 1, stmt, populatedColumns() );
	}
	
	public int populateStatement( int i, PreparedStatement stmt, List<Column> cols) throws SQLException
	{
		for ( Column col : cols )
		{
			col.populate( i++, stmt );
		}
		return i;
	}

	public long executeInsert( PreparedStatement stmt ) throws SQLException
	{
		long key = -1;
		stmt.executeUpdate();
		if ( hasPrimaryKey() )
		{
			ResultSet rs = stmt.getGeneratedKeys();
			if ( rs.next() )
			{
				key = rs.getLong( 1 );
				primaryKey().setValue( key );
			}
			rs.close();
		}
		stmt.close();
		return key;
	}

	public String toWhereClause( List<Column> cols )
	{
		StringBuffer sb = new StringBuffer();
		sb.append( " WHERE " );
		boolean first = true;
		for ( Iterator iterator = cols.iterator(); iterator.hasNext(); )
		{
	
			Column column = (Column) iterator.next();
			if ( !first )
			{
				sb.append( " AND " );
			}
			sb.append( column.getName() + " = ?" );
			first = false;
		}
		return sb.toString();
	}

	public String toWhereClause( Column col )
	{
		List<Column> cols = new ArrayList<Column>();
		cols.add( col );
		return toWhereClause( cols );
	}

}
