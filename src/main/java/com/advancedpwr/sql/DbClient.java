package com.advancedpwr.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class DbClient<T extends DbRecord>
{
	private static final Logger LOGGER = LoggerFactory.getLogger( DbClient.class );
	
	protected Connection fieldConnection;

	protected DatabaseConnectionProvider fieldProperties;
	
	protected String fieldOrderBy;
	
	protected String fieldWhereClause;
	
	protected boolean fieldHold;
	
	public void holdConnection()
	{
		fieldHold = true;
	}
	
	public void releaseConnection()
	{
		fieldHold = false;
	}

	public Connection getConnection() throws SQLException
	{
		if ( fieldConnection == null || fieldConnection.isClosed() )
		{
			fieldConnection = createConnection();
		}

		return fieldConnection;
	}

	public void setConnection( Connection inConnection )
	{
		fieldConnection = inConnection;
	}
	
	protected Connection createConnection() throws SQLException
	{
		return getProperties().getConnection();
	}

	public DatabaseConnectionProvider getProperties()
	{
		return fieldProperties;
	}

	public void setProperties( DatabaseConnectionProvider properties )
	{
		fieldProperties = properties;
	}

	public void closeConnection() throws SQLException
	{
		if ( fieldHold )
		{
			return;
		}
		getConnection().close();
	}

	public PreparedStatement prepareStatement( String inSql ) throws SQLException
	{
		LOGGER.debug( inSql );
		DbRecord record = newInstance();
		if ( record.primaryKey() != null )
		{
			return getConnection().prepareStatement( inSql, new String[] { record.primaryKey().getName() } );
		}
		return getConnection().prepareStatement( inSql );
	}

	public PreparedStatement prepareSelectStatement( String inWhereClauseSql ) throws SQLException
	{
		return prepareStatement( selectSql( inWhereClauseSql ) );
	}

	protected String selectSql( String inWhereClauseSql )
	{
		return "select * from " + tableName() + " " + inWhereClauseSql;
	}

	protected String tableName()
	{
		return newInstance().tableName();
	}
	
	public long insertRecord( T inData ) throws SQLException
	{
		dateStamp( inData );
		createdStamp( inData );
		SqlGenerator generator = createSqlGenerator();
		generator.setData( inData );
		long key;
		try
		{
			PreparedStatement stmt = prepareStatement( generator.toInsert() );
			generator.populateStatement( stmt );
			key = executeInsert( generator, stmt );
		}
		catch ( SQLException e )
		{
			throw e;
		}
		finally
		{
			closeConnection();
		}
		return key;
	}

	protected SqlGenerator createSqlGenerator()
	{
		return new SqlGenerator();
	}

	public void insertBatch( List<T> items ) throws SQLException
	{
		SqlGenerator generator = createSqlGenerator();
		generator.setData( newInstance() );
		try
		{
			PreparedStatement stmt = getConnection().prepareStatement( generator.toInsert() );
			for ( T lineItem : items )
			{
				dateStamp( lineItem );
				createdStamp( lineItem );
				generator.setData( lineItem );
				generator.populateStatement( stmt );
				stmt.addBatch();
			}
			stmt.executeBatch();
		}
		catch ( SQLException e )
		{
			throw e;
		}
		finally 
		{
			closeConnection();
		}
	}
	
	protected long executeInsert( SqlGenerator generator, PreparedStatement stmt )
			throws SQLException
	{
		return generator.executeInsert( stmt );
	}
	
	public void update( T inData ) throws SQLException
	{
		dateStamp( inData );
		SqlGenerator generator = createSqlGenerator();
		generator.setData( inData );
		try
		{
			PreparedStatement stmt = prepareStatement( generator.toUpdate() + generator.toWhereClause( inData.primaryKey() ) );
			int index = generator.populateStatement( stmt );
			stmt.setObject( index, inData.primaryKey().getValue() );
			stmt.execute();
		}
		catch ( SQLException e )
		{
			throw e;
		}
		finally
		{
			closeConnection();
		}
	}

	protected void dateStamp( T inData )
	{
		if ( DateStamped.class.isAssignableFrom( inData.getClass() ) )
		{
			((DateStamped)inData).setLastUpdated( new Date() );
		}
	}
	
	public void update( T inData, T inTemplate ) throws SQLException
	{
		SqlWhereGenerator whereGenerator = createSqlWhereGenerator();
		whereGenerator.setData( inTemplate );
		update( inData, whereGenerator.populatedColumns() );
	}
	
	public void update( T inData, List<Column> whereList ) throws SQLException
	{
		dateStamp( inData );
		
		SqlGenerator generator = createSqlGenerator();
		generator.setData( inData );
		try
		{
			PreparedStatement stmt = prepareStatement( generator.toUpdate() + generator.toWhereClause( whereList ) );
			int index = generator.populateStatement( stmt );
			generator.populateStatement( index, stmt, whereList );
			stmt.execute();
		}
		catch ( SQLException e )
		{
			throw e;
		}
		finally
		{
			closeConnection();
		}
	}
	
	public void update( T inData, Column whereColumn ) throws SQLException
	{
		List<Column> columns = new ArrayList<Column>();
		columns.add( whereColumn );
		update( inData, columns );
	}

	protected void createdStamp( T inData )
	{
		if ( CreatedStamped.class.isAssignableFrom( inData.getClass() ) )
		{
			CreatedStamped stamped = (CreatedStamped) inData;
			if ( stamped.getCreatedDate() == null )
			{
				((CreatedStamped)inData).setCreatedDate( new Date() );
			}
		}
	}
	
	public T load( long inId ) throws SQLException
	{
		DbObject template = new DbObject();
		template.primaryKey().setValue( inId );
		return loadWhere( template );
	}
	
	public void delete( long inId ) throws SQLException
	{
		try
		{
			PreparedStatement stmt = prepareStatement( "delete from " + tableName() + " where id = ?" );
			stmt.setLong( 1, inId );
			stmt.execute();
		}
		catch ( SQLException e )
		{
			throw e;
		}
		finally
		{
			closeConnection();
		}
	}
	
	public T loadWhere( DbRecord template ) throws SQLException
	{
		try
		{
			ResultSet rs = whereResults( template );
			if ( rs.next() )
			{
				T object = newInstance();
				populate( object, rs );
				return object;
			}
			return notFound();
		}
		finally
		{
			closeConnection();
		}
	}

	protected T notFound()
	{
		return null;
	}
	protected ResultSet whereResults( DbRecord template ) throws SQLException
	{
		SqlWhereGenerator generator = createSqlWhereGenerator();
		generator.setData( template );
		String where = generator.toWhereClause();
		if ( getWhereClause() != null )
		{
			where = getWhereClause();
		}
		PreparedStatement stmt = prepareSelectStatement(  where + orderByClause() );
		generator.populateStatement( stmt );
		ResultSet rs = stmt.executeQuery();
		return rs;
	}

	protected SqlWhereGenerator createSqlWhereGenerator()
	{
		return new SqlWhereGenerator();
	}
	
	public List<T> loadAllWhere( DbRecord template ) throws SQLException
	{
		List<T> list = new ArrayList<T>();
		try
		{
			ResultSet rs = whereResults( template );
			while( rs.next() )
			{
				T object = newInstance();
				populate( object, rs );
				list.add( object );
			}
			return list;
		}
		finally
		{
			closeConnection();
		}
	}

	protected abstract T newInstance();
	
	protected void populate( DbRecord inObject, ResultSet rs )
	{
		SqlLoader loader = new SqlLoader();
		loader.setData( inObject );
		loader.setResultSet( rs );
		loader.populate();
	}

	public String getOrderBy()
	{
		return fieldOrderBy;
	}

	public void setOrderBy( String orderBy )
	{
		fieldOrderBy = orderBy;
	}
	
	protected String orderByClause()
	{
		if ( getOrderBy() == null )
		{
			return "";
		}
		return " " + getOrderBy();
	}

	public String getWhereClause()
	{
		return fieldWhereClause;
	}

	public void setWhereClause( String whereClause )
	{
		fieldWhereClause = whereClause;
	}
}
